

variable "region" {}


variable "access_key" {
  description = "Access key to AWS console"
}
variable "secret_key" {
  description = "Secret key to AWS console"
}

variable "aws_session_token" {
  description = "Temporary session token used to create instances"
}

